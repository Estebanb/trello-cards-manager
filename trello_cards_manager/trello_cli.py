#!/usr/bin/python3
"""
Trello Cards Manager CLI

This file implements the trello lib and provides a command line interface to create Trello Cards.
"""
import sys
import argparse
import os.path
from trello_cards_manager.trello_lib import TrelloCli


def main() -> int:
    """Main Trello CLI"""
    parser = argparse.ArgumentParser(description='Trello Cards Manager CLI')
    parser.add_argument('--config-file', action='store', default='user_data.yaml',
                      help='Configuration file for the CLI.')
    parser.add_argument('--csv-card', action='store',
                      help='CSV file with the Card future contents.')
    args = parser.parse_args()
    trello_cli = TrelloCli()
    if os.path.isfile(args.config_file):
        trello_cli.load_config_from_file(args.config_file)
    else:
        print(f'Configuration file not found in:{args.config_file}\n'
              'Please input the values manually, they will be saved in the provided path.')
        trello_cli.load_config_from_user_input()
        trello_cli.save_config_to_file(args.config_file)

    print(args.csv_card)
    trello_cli.create_new_card(args.csv_card)
    return 0

if __name__ == '__main__':
    sys.exit(main())

"""
Trello Cards Manager library

This file contains all the necessary methods to create Trello Cards.
"""
import csv
import yaml
import requests
import json


cards_url = "https://api.trello.com/1/cards"
headers = {
   "Accept": "application/json"
}


VALID_BOT_KEY_LEN = 32
VALID_USER_TOKEN_LEN = 64

class InvalidBotKey(Exception):
    """Exception raised when bot key len is not the expected one."""
    def __init__(self, key_length, message=None):
        if not message:
            self.message = f'Invalid bot key len:{key_length}, key len must be:{VALID_BOT_KEY_LEN}'
        else:
            self.message = message
        super().__init__(self.message)

class InvalidUserToken(Exception):
    """Exception raised when user token len is not the expected one."""
    def __init__(self, key_length, message=None):
        if not message:
            self.message = f'Invalid user token len:{key_length}, key len must be:{VALID_USER_TOKEN_LEN}'
        else:
            self.message = message
        super().__init__(self.message)


class TrelloCli:
    """Class in charge of interact with the Trello Rest API"""
    def __init__(self):
        self.bot_key = ''
        self.user_token = ''

    def _validate_config(self):
        if len(self.bot_key) != VALID_BOT_KEY_LEN:
            raise InvalidBotKey(len(self.bot_key))
        if len(self.user_token) != VALID_USER_TOKEN_LEN:
            raise InvalidUserToken(len(self.user_token))

    def load_config_from_user_input(self):
        """Loads the configuration from the command line"""
        print("")
        self.bot_key = input("Enter the bot key:")
        self.user_token = input("Enter the user token:")
        self._validate_config()

    def load_config_from_file(self, filename):
        """Loads the configuration from a yaml file"""
        with open(filename, 'r') as stream:
            config = yaml.safe_load(stream)
        self.bot_key = config['trello']['bot_key']
        self.user_token = config['trello']['user_token']
        self._validate_config()

    def save_config_to_file(self, filename):
        """Saves the configuration in to a yaml file"""
        data = {}
        data['trello'] = {}
        data['trello']['bot_key'] = self.bot_key
        data['trello']['user_token'] = self.user_token
        with open(filename, 'w') as stream:
            yaml.dump(data, stream)

    def create_new_card(self, csv_card=None):
        """Creates a new card, ifthe user provides a csv file the fields will be read from that filename
        the csv file columns should be: idList, name, description.
        """
        if csv_card:
            with open(csv_card, 'r') as stream:
                reader = csv.reader(stream)
                list_id, name, desc = reader[1]
        else:
            print("Please provide the data to create the new card:")
            list_id = input("idList:")
            name = input("Card name:")
            desc = input("Card description:")

        query = {'idList': list_id, 'name': name, 'desc': desc, 'key': self.bot_key, 'token':self.user_token}
        response = requests.request(
            "POST",
            cards_url,
            headers=headers,
            params=query)
        if response.status_code != 200:
            print(f"Something went wrong creating the card, response:{response}")
        else:
            print(f"Congratulations, the card {name} was created successfully")

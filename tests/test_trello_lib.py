import unittest
import yaml
from unittest.mock import patch
import unittest.mock as umock
from trello_cards_manager.trello_lib import TrelloCli, InvalidBotKey, InvalidUserToken

class TestTrelloLibConfig(unittest.TestCase):

    @patch('trello_cards_manager.trello_lib.input', side_effect=['12345678901234567890123456789012','1234567890123456789012345678901212345678901234567890123456789012'])
    def test_load_config_from_user_input_when_no_error(self, input):
        trello_cli = TrelloCli()
        trello_cli.load_config_from_user_input()

    @patch('trello_cards_manager.trello_lib.input', side_effect=['1234567890123456789012345678901','1234567890123456789012345678901212345678901234567890123456789012'])
    def test_load_config_from_user_input_when_bot_key_wrong_len(self, input):
        trello_cli = TrelloCli()
        self.assertRaises(InvalidBotKey, trello_cli.load_config_from_user_input)

    @patch('trello_cards_manager.trello_lib.input', side_effect=['12345678901234567890123456789012','123456789012345678901234567890121234567890123456789012345678901'])
    def test_load_config_from_user_input_when_user_token_wrong_len(self, input):
        trello_cli = TrelloCli()
        self.assertRaises(InvalidUserToken, trello_cli.load_config_from_user_input)

    def test_load_config_from_file_when_no_error(self):
        trello_cli = TrelloCli()
        data = """trello:
                    bot_key: a2345678901234567890123456789012
                    user_token: a123456789012345678901234567890121234567890123456789012345678901
               """
        with umock.patch('trello_cards_manager.trello_lib.open', umock.mock_open(read_data=data)):
            trello_cli.load_config_from_file("some_name")

    def test_load_config_from_file_when_bot_key_wrong_len(self):
        trello_cli = TrelloCli()
        data = """trello:
                    bot_key: a234567890123456789012345689012
                    user_token: a123456789012345678901234567890121234567890123456789012345678901
               """
        with umock.patch('trello_cards_manager.trello_lib.open', umock.mock_open(read_data=data)):
            self.assertRaises(InvalidBotKey, trello_cli.load_config_from_file, "some_name")

    def test_load_config_from_file_when_token_wrong_len(self):
        trello_cli = TrelloCli()
        data = """trello:
                    bot_key: a2345678901234567890123451689012
                    user_token: a12345678901234567890234567890121234567890123456789012345678901
               """
        with umock.patch('trello_cards_manager.trello_lib.open', umock.mock_open(read_data=data)):
            self.assertRaises(InvalidUserToken, trello_cli.load_config_from_file, "some_name")



if __name__ == '__main__':
    unittest.main()

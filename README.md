# Trello Cards Manager
The following project provides the capabilty to create Trello Cards.

This is a demo project and only the "creation" of Cards is implemented.

# Installation:

## Pip Package:
### Downloading the repo:
Clone the repo:
```
git clone https://gitlab.com/Estebanb/trello-cards-manager.git
```
Install the package:
```
cd trello_cards_manager && pip install .
```
### Whithout downloading the repo:
```
pip install git+https://gitlab.com/Estebanb/trello-cards-manager.git
```

# Execute the application:
```
trello_cli
```

## Debian package:
Download it from the repo and install it:
```
wget https://gitlab.com/Estebanb/trello-cards-manager/-/jobs/1825772725/artifacts/raw/trellocardsmanager_0.0.1_all.deb 
sudo dpkg -i trellocardsmanager_0.0.1_all.deb
```

## Run application in docker:
```
cd examples
docker build -t trello-container .
docker run -it trello-container
```


### Possible improvements:
* Fix dependencies, some dependencies are missing in the debian control (Is possible to find them in the dockerfile).
* Variables like the release version are hardcoded.
* Some debian files are not fully reviewed, they are just generated (ex:changelog).
* CI/CD speed could be improved easly generating docker impages with the dependencies and pushing them to the regisitry.
* Debhelper is probably not using the last version available.
* Fields in the setup.py are not properly completed.
* The actual implementation of the card manager is very limited and only offers the option to create cards, delete, update, etc are missing.

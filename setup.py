from setuptools import setup

setup(name='trello_cards_manager',
      version='0.1',
      description='Package to create trello cards',
      url='https://gitlab.com/Estebanb/trello-cards-manager',
      author='Esteban Bosse',
      author_email='estebanbosse@gmail.com',
      license='MIT',
      packages=['trello_cards_manager'],
      zip_safe=False,
      include_package_data=True,
      entry_points="""
      [console_scripts]
      trello_cli=trello_cards_manager.trello_cli:main
      """
)

